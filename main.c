#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <string.h>
#include <string.h>
#include "bmp.h"

int main()
{
	
	char inPath[100], outPath[100];
	printf("Input path to picture: "); scanf("%s",inPath);
	
	FILE *in, *out;
	if((in = fopen(inPath, "rb"))==NULL)
	{
		printf("Error: error opening the file");
		return 0;
	}
		
	
	
	struct image* img = (struct image*)malloc(sizeof(struct image));
	
	enum read_status rs = from_bmp(in, img);
	fclose(in); 
	switch(rs)
	{
		case 1: printf("Error: Bad path"); return 0;
		case 2: printf("Error: Bad structure of picture"); return 0;
		case 3: printf("Error: Bad header"); return 0;
	}
	
	
	*img = rotate(*img);
	
	out = fopen(".\\newImg.bmp", "wb");
	enum write_status ws = to_bmp(out, img);
	fclose(out);
	
	if(rs==1)
	{
		printf("Error: error writing file");
		return 0;
	}
	
	
	return 0;
}