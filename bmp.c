#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <string.h>
#include <string.h>
#include "image.h"

struct __attribute__((packed)) bmp_header
{
	uint16_t bfType;
	uint32_t  bfileSize;
	uint32_t bfReserved;
	uint32_t bfOffBits;
	
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t  biHeight;
	uint16_t  biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t  biClrImportant;
};

void printHeader(struct bmp_header h)
{
	printf("bfType          %" PRIu16, h.bfType);
	printf("\nbfileSize       %" PRIu32, h.bfileSize);
	printf("\nbfReserved      %" PRIu32, h.bfReserved);
	printf("\nbfOffBits       %" PRIu32, h.bfOffBits);
	printf("\n");
	
	printf("\nbiSize          %" PRIu32, h.biSize);
	printf("\nbiWidth         %" PRIu32, h.biWidth);
	printf("\nbiHeight        %" PRIu32, h.biHeight);
	printf("\nbiPlanes        %" PRIu16, h.biPlanes);
	printf("\nbiBitCount      %" PRIu16, h.biBitCount);
	printf("\nbiCompression   %" PRIu32, h.biCompression);
	printf("\nbiSizeImage     %" PRIu32, h.biSizeImage);
	printf("\nbiXPelsPerMeter %" PRIu32, h.biXPelsPerMeter);
	printf("\nbiYPelsPerMeter %" PRIu32, h.biYPelsPerMeter);
	printf("\nbiClrUsed       %" PRIu32, h.biClrUsed);
	printf("\nbiClrImportant  %" PRIu32, h.biClrImportant);
	
}

size_t make_padding(uint64_t width)
{
	return (4-(width*3)%4)%4;
}

int8_t* make_garbage(uint64_t width)
{
	return (int8_t*)malloc(make_padding(width));
}

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};

    
enum read_status from_bmp( FILE* in, struct image* img )
{
	struct bmp_header header;
	fread(&header, sizeof(struct bmp_header),1,in);
	
	int8_t* palitra = (int8_t*)malloc((size_t)header.bfOffBits-sizeof(struct bmp_header));
	fread(palitra, 1, (size_t)header.bfOffBits - sizeof(struct bmp_header),in);	
	
	//printHeader(header);
	
	uint64_t width = (uint64_t)header.biWidth, height = (uint64_t)header.biHeight;
	
	struct pixel* data = (struct pixel*)malloc(sizeof(struct pixel)*((size_t)(width*height)));
	
	for(uint64_t i=height-1;i>=0;i--)
	{
		fread(data+(size_t)(i*width), sizeof(struct pixel), (size_t)width, in);
		fread(make_garbage(width), 1, (size_t)make_padding(width), in);
		if(i==0)
			break;
	}
	
	*img = (struct image){.width=width, .height=height, .data=data};

	return 0;
}

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

struct bmp_header make_header(struct image const* img)
{
	uint64_t width = img->width, height = img->height;
	return (struct bmp_header)
	{
		.bfType = 19778,
		.bfileSize = sizeof(struct bmp_header)+(width*3 + (4-(3*width)%4)%4)*height,
		.bfReserved = 0,
		.bfOffBits = sizeof(struct bmp_header),
	
		.biSize = sizeof(struct bmp_header)-14,
		.biWidth = width,
		.biHeight = height,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = (width*3 + (4-(3*width)%4)%4)*height,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0
	};
}

enum write_status to_bmp( FILE* out, struct image const* img )
{
	uint64_t width = img->width, height = img->height;
	
	struct bmp_header header = make_header(img);
	
	size_t count = 0;
	//printHeader(header);
	
	count += fwrite(&header, sizeof(struct bmp_header), 1, out);
	
	int8_t* palitra = (int8_t*)malloc((size_t)header.bfOffBits-sizeof(struct bmp_header));
	count += fwrite(palitra, 1, (size_t)header.bfOffBits - sizeof(struct bmp_header),out);

	struct pixel* data = img->data;
	
	for(uint64_t i=height-1;i>=0;i--)
	{
		count += fwrite(data+(size_t)(i*width), sizeof(struct pixel), (size_t)width, out);
		count += fwrite(make_garbage(width), 1, (size_t)make_padding(width), out);
		if(i==0)
			break;
	}
	
	if(count!=(size_t)header.bfileSize)
		return 1;
	return 0;
		
}
