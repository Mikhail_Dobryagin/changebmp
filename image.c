#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <string.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source )
{
	uint64_t width = source.width, height = source.height;
	
	struct pixel* data = (struct pixel*)malloc(sizeof(struct pixel)*(size_t)(width*height));
	
	for(uint64_t i=0;i<height;i++)
		for(uint64_t j =0;j<width;j++)
			data[j*height+height-i-1]=source.data[i*width+j];
		
	return (struct image){.width=height, .height=width, .data=data};
}